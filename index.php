<html

<!-- ************************** EXERCICIO 02 ************************ -->
<?php
/**
 * Created by PhpStorm.
 * User: Joao Ignacio
 * Date: 10/08/2017
 * Time: 21:19
 */
/**
 * @param $quantidade
 * @return int
 */
function teste($quantidade){
    $mariana = "Mariana conta ";
    for($i=1; $i<=$quantidade; $i++) :
        echo "$mariana $i<br>";
        echo "$mariana $i";
        for ($j=1; $j<=$i; $j++) :
            echo " é $j";
        endfor;
        echo ",<br>";
        $viva = "viva a Mariana";
        echo "Ana $viva $viva.<br><br>";
    endfor;
    //return(0);
}
?>
<?php echo teste(1); ?>
<?= teste(1); ?>


<!-- ************************** EXERCICIO 03 ************************ -->
<?php
/**
 * Created by PhpStorm.
 * User: Joao Ignacio
 * Date: 17/08/2017
 * Time: 21:19
 */
/**
 * @param $array
 */
function imprimir_matriz_2d_tabela($array){

    if( !empty($array) ){ // se o array não estiver vazio

        $linhas = sizeof($array);
        $colunas = sizeof($array[0]);

        echo "<table border='1' cellpadding='0' cellspacing='0'>"; // abre tabela
        echo "<tr>"; // abre cabeçalho da tabela

        for( $j = 0 ; $j < $colunas ; $j++ )
            echo "<th>Coluna $j</th>"; // insere colunas no cabeçalho

        echo "</tr>"; // fecha cabeçalho da tabela

        // imprimir array
        for( $i = 0 ; $i < $linhas ; $i++ ) {

            echo "<tr>"; // abre a linha

            for ($j = 0; $j < $colunas; $j++){
                echo "<td>"; // abre cada uma das colunas $j dentro da linha $i
                echo $array[$i][$j]; // insere valor do array nas colunas
                echo "</td>"; // fecha a coluna para
            }

            echo "</tr>";// fecha a linha

        }// se ainda houver linhas $i, repete

        echo "</table>"; // fecha a tabela

    } // fim do if array não vazio

}// fim da função
$dados = array(
    array(0,"João","joao@i.br", "123456", 'm',1),
    array(1,"Maria","maria@i.br","654321","f",1),
    array(2,"Gabriela","gabilimnda@i.br","password","f",1),
    array(3,"Kurt","cobain@i.br","teenspirit","m",0),
    array(4,"James","hetfield@i.br","seekanddestroy","m",0)
);
//call_user_func("imprimir_matriz_2d_tabela",$dados);
imprimir_matriz_2d_tabela($dados);
?>

<!-- ************************** EXERCICIO 04 ************************ -->
<br><br><br><br>
<form action="<?= $_SERVER['PHP_SELF']?>" method="POST">
    <label for="nome">Nome</label><br>
    <input type="text" name="nome" id="nome"><br>
    <br>
    <label for="estCivil">Estado Civil</label><br>
    <input type="radio" name="optEstCivil" id="optEstCivil" value="Casado" checked>Casado<br>
    <input type="radio" name="optEstCivil" id="optEstCivil" value="Solteiro" >Solteiro<br>
    <br>
    <select name="estado" id="estado">
        <option value=" (opção não declarada) ">Selecionar UF</option>
        <option>ES</option>
        <option>MG</option>
        <option>RJ</option>
        <option>SP</option>
    </select>
    <br>
    <select name="mesNascimento" id="mesNascimento">
        <option value="0">Mês</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        <option>6</option>
        <option>7</option>
        <option>8</option>
        <option>9</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>
    </select>
    <br>
    <label for="casa">Selecione os extras</label><br>
    <input type="checkbox" name="extras[]" value="Piscina" checked>Piscina<br>
    <input type="checkbox" name="extras[]" value="Estacionamento" >Estacionamento<br>
    <input type="checkbox" name="extras[]" value="Carro" >Carro<br>
    <br>
    <input type="submit" value="Enviar">
</form><br>

<?php


function mesNascimento($numero){
    switch ($numero){
        case 1 : return "janeiro";
        case 2 : return "fevereiro";
        case 3 : return "março";
        case 4 : return "abril";
        case 5 : return "maio";
        case 6 : return "junho";
        case 7 : return "julho";
        case 8 : return "agosto";
        case 9 : return "setembro";
        case 10 : return "outubro";
        case 11 : return "novembro";
        case 12 : return "dezembro";
        default : return "mês não declarado";
    }
}

if( isset( $_POST['nome'], $_POST['optEstCivil'], $_POST['estado'], $_POST['mesNascimento'], $_POST['extras'] ) ){
    //echo "O nome enviado foi ";
    echo $_POST['nome']." é ".$_POST['optEstCivil']." e reside no estado de ".$_POST['estado'];
    echo ", nascido em ".mesNascimento($_POST['mesNascimento']);
    echo ", também possui: ";
    echo "<ul>";
    foreach ($_POST['extras'] as $k){
        echo "<li>$k</li>";
    }

}


?>


<?php
require_once ("ContaPoupanca.php"); // import class Conta

$poupança = new ContaPoupanca(90, 1,006);
$poupança->depositar(10);
$poupança->rendimento();
echo $poupança->getSaldo();

