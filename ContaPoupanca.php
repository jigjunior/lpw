<?php
require_once ("Conta.php");

class ContaPoupanca extends Conta
{
    public $taxaRendimento;

    function rendimento(){
        $this->saldo *= $this->taxaRendimento;
    }

    function __construct($saldoInicial, $taxaRendimento)
    {
        parent::__construct($saldoInicial);
        $this->taxaRendimento = $taxaRendimento;
    }

}