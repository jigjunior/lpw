<?php

class Conta{
    public $saldo;

    function depositar($valor){
        $this->saldo += $valor;
    }

    function sacar($valor){
        $this->saldo -= $valor;
    }

    function __construct($saldoInicial)
    {
        $this->saldo = $saldoInicial;
    }

    function getSaldo(){
        return $this->saldo;
    }

}

